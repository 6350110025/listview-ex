import 'package:flutter/material.dart';

class Example2 extends StatelessWidget {
  const Example2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ListView 2'),
      ),
      body: ListView(
        children: [
          ListTile(
            leading: Icon(Icons.directions_railway,
            size: 42,
            ),
            title: Text(
              '8.00 A.M.',
              style: TextStyle(fontSize: 18,),
            ),
            subtitle: Text(
              'Now : Trang - Bangkok',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
                Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print('Train');
            },
          ),
          ListTile(
            leading: Icon(Icons.directions_bike,
              size: 42,
            ),
            title: Text(
              '6.00 A.M.',
              style: TextStyle(fontSize: 18,),
            ),
            subtitle: Text(
              'Up Coming : PSU TRANG, Sports',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print('Driving');
            },
          ),
          ListTile(
            leading: Icon(Icons.directions_boat,
              size: 42,
            ),
            title: Text(
              '8.00 A.M.',
              style: TextStyle(fontSize: 18,),
            ),
            subtitle: Text(
              'Up Coming : Trang - Phuket',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print('Driving');
            },
          ),
          ListTile(
            leading: Icon(Icons.directions_car_rounded,
              size: 42,
            ),
            title: Text(
              '8.00 A.M.',
              style: TextStyle(fontSize: 18,),
            ),
            subtitle: Text(
              'Up Coming : Start at Trang',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print('Driving');
            },
          ),
          ListTile(
            leading: Icon(Icons.directions_run,
              size: 42,
            ),
            title: Text(
              '8.00 A.M.',
              style: TextStyle(fontSize: 18,),
            ),
            subtitle: Text(
              'Up Coming : PSU TRANG, Sports',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print('Running');
            },
          ),
          ListTile(
            leading: Icon(Icons.directions_walk_sharp,
              size: 42,
            ),
            title: Text(
              '8.00 A.M.',
              style: TextStyle(fontSize: 18,),
            ),
            subtitle: Text(
              'Up Coming : PSU TRANG, Domitory 1',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print('Walking');
            },
          ),
        ],
      ),
    );
  }
}
