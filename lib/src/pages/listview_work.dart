import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ListView_Work extends StatelessWidget {
  ListView_Work({Key? key}) : super(key: key);

  final titles = ['SnookKunGz', 'Natcharin K.', 'Serny', 'O-Jiang',
    'KoKo San', 'ChocoBears', 'Chocochic Store', 'Garena RoV Thailand', 'Grills by Chocochic'];

  final imgs = [
    CircleAvatar(backgroundImage: AssetImage('images/profile1.png'),),
    CircleAvatar(backgroundImage: AssetImage('images/profile2.png'),),
    CircleAvatar(backgroundImage: AssetImage('images/profile3.png'),),
    CircleAvatar(backgroundImage: AssetImage('images/profile4.png'),),
    CircleAvatar(backgroundImage: AssetImage('images/profile5.png'),),
    CircleAvatar(backgroundImage: AssetImage('images/profile6.png'),),
    CircleAvatar(backgroundImage: AssetImage('images/profile7.png'),),
    CircleAvatar(backgroundImage: AssetImage('images/profile8.png'),),
    CircleAvatar(backgroundImage: AssetImage('images/profile9.png'),),
  ];

  final subs = ['@snook_suebsakun','@nat_natcharin','@serny_ahc','@ojiang_ccb','@koko_ccb','@chocobears','@chocochicstore','@garena_rov_thailand','@grills_chocochic'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ListView Work (Social)'),
      ),
      body: ListView.builder(
          itemCount: titles.length,
          itemBuilder: (context,index){
            return Column(
              children: [
                ListTile(
                  leading: CircleAvatar(child: imgs[index]),
                  title: Text('${titles[index]}',style: TextStyle(fontSize: 18),),
                  subtitle: Text('${subs[index]}',style: TextStyle(fontSize: 15),),
                  trailing: Icon(Icons.notifications_none, size: 25,), onTap: (){


                  Fluttertoast.showToast(
                      msg: "Followed ${titles[index]} successfully. 😚",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIosWeb: 3,
                      backgroundColor: Colors.green,
                      textColor: Colors.white,
                      fontSize: 16.0
                  );

                },
                ),
                Divider(thickness: 1,),
              ],
            );
          }
      ),
    );
  }
}
